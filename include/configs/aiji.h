/*
 * Configuation settings for the MagicEyes POLLUX board.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.		See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#define DEBUG

/*-----------------------------------------------------------------------
 * High Level Configuration Options
 * (easy to change)
 */
#define CONFIG_ARM926EJS		1	/* This is an arm926ejs CPU core	*/
#define CONFIG_POLLUX			1	/* in a MagicEyes POLLUX SoC  	 	*/
#define CONFIG_POLLUX_LF		1	/* in a MagicEyes POLLUX LF Board 	*/
#define CONFIG_LCD_LTE430WQ 	1

#undef CONFIG_POLLUX_SHADOW_ONE

#ifdef  CONFIG_LCD_LTE430WQ
//#define CONFIG_LCD_24BIT		1
#endif	


/*-----------------------------------------------------------------------
 * input clock of PLL 
 */
#define CONFIG_SYS_CLK_FREQ		27000000	/* input clock			*/
#undef CONFIG_USE_IRQ			     		/* we don't need IRQ/FIQ stuff	*/

/*-----------------------------------------------------------------------
 * Size of malloc() pool
 */
#define CFG_MALLOC_LEN			(CFG_ENV_SIZE + 64*1024)/* unit is kbyte */
#define CFG_GBL_DATA_SIZE		128	     		/* size in bytes reserved for initial data */

/*-----------------------------------------------------------------------
 * Hardware drivers
 */
#define CONFIG_TIMER_ENABLE	   	1
#define CFG_HZ                  (147000000/25)	/* TIMERSELPLL(PLL1:147Mhz)/TIMREDIV(25) */

/*
#define CONFIG_DRIVER_CS8900		1
#define CS8900_BASE	   				0x8c000000
#define CS8900_BUS16				1
*/

#define CONFIG_DRIVER_DM9000A		1
#define DM9000_BASE					0x04000000 /* cs1, shadow=0 */


/*-----------------------------------------------------------------------
 * select serial console configuration
 */
#define CONFIG_DRIVER_POLLUX_SERIAL   	1

#define CONFIG_SERIAL1		   	1
#define CONFIG_CONS_INDEX	   	1
#define CONFIG_BAUDRATE		   	115200
#define CFG_BAUDRATE_TABLE	   	{ 9600, 19200, 38400, 57600, 115200 }
#define CONFIG_COMMANDS         	(CFG_CMD_BDI    | \
                                 	 CFG_CMD_BOOTD  | \
                                 	 CFG_CMD_DHCP   | \
                                 	 CFG_CMD_ENV    | \
                                 	 CFG_CMD_FLASH  | \
	                                 CFG_CMD_NAND   | \
        	                         CFG_CMD_IMI    | \
	                                 CFG_CMD_LOADB  | \
        	                         CFG_CMD_NET    | \
                	                 CFG_CMD_MEMORY | \
                        	         CFG_CMD_PING   | \
                                	 CFG_CMD_RUN)


#define CONFIG_BOOTP_MASK	   	CONFIG_BOOTP_DEFAULT

/*-----------------------------------------------------------------------
 * This must be included AFTER the definition of CONFIG_COMMANDS (if any)
 */
#include <cmd_confdefs.h>

#define CONFIG_BOOTDELAY	   	3
//#define CONFIG_ETHADDR		0a:0a:0a:0a:0a:0a:0a
#define CONFIG_ETHADDR		   	11:00:33:22:55:44:66
#define CONFIG_NETMASK		   	255.255.255.0

#define CONFIG_BOOTARGS 	"mem=64M root=/dev/nfs rw nfsroot=192.168.0.250:/root/gwork/rootfs ip=192.168.0.100:192.168.0.250:192.168.0.1:255.255.255.0:godori:eth0:off console=ttySAC0,115200n81 ethaddr=08:00:3e:26:0a:5b"
#define CONFIG_LOADADDR			0x01000000
#define CONFIG_IPADDR			192.168.0.100
#define CONFIG_SERVERIP			192.168.0.250		
#define CONFIG_GATEWAYIP		192.168.0.1
#define CONFIG_BOOTFILE			"uImage"  	// File to load
#define CONFIG_BOOTCOMMAND		"tftp 0x82000000 uImage; bootm 0x82000000"
#define CFG_PROMPT				"gpollux# "     /* Monitor Command Prompt   */


/*-----------------------------------------------------------------------
 * Miscellaneous configurable options
 */
#define CFG_LONGHELP				       				/* undef to save memory	   */
#define CFG_CBSIZE		   	256		      				/* Console I/O Buffer Size  */
#define CFG_PBSIZE		   	(CFG_CBSIZE+sizeof(CFG_PROMPT)+16)	/* Print Buffer Size */
#define CFG_MAXARGS			16		       					/* max number of command args   */
#define CFG_BARGSIZE			   CFG_CBSIZE	       			/* Boot Argument Buffer Size    */
	
#define CFG_MEMTEST_START		   0x80000000	       			/* memtest works on */
#define CFG_MEMTEST_END			   0x84000000	       			/* 64 MB in DRAM	   */

#undef CFG_CLKS_IN_HZ		     								/* everything, incl board info, in Hz */
#define CFG_LOAD_ADDR			   0x82000000	       			/* default load address, 32M offset */

/*-----------------------------------------------------------------------
 * Platform Board Specific 
 *
 */
#define	CONFIG_MISC_INIT_R
 
/*-----------------------------------------------------------------------
 * Stack sizes
 *
 * The stack sizes are set up in start.S using the settings below
 */
#define CONFIG_STACKSIZE		   	(128*1024)	  /* regular stack */

#ifdef  CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ	  	 	(4*1024)	  /* IRQ stack */
#define CONFIG_STACKSIZE_FIQ	   		(4*1024)	  /* FIQ stack */
#endif

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */
#define CONFIG_NR_DRAM_BANKS	   		1
#define PHYS_SDRAM_1		   		0x80000000	  /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE	   		0x04000000	  /* 64 MB */

/*-----------------------------------------------------------------------
 * FLASH and environment organization
 */
//#define CFG_NO_FLASH				1 // only nand

#define PHYS_FLASH_1		0x00000000 /* cs0, shadow=0 */
#define CFG_FLASH_BASE		PHYS_FLASH_1

#define CONFIG_AMD_LV320	1	
#define CFG_MAX_FLASH_BANKS	1	/* max number of memory banks */

#define PHYS_FLASH_SIZE		0x00400000 /* 4MB */
#define CFG_MAX_FLASH_SECT	(71)	/* max number of sectors on one chip */
#define CFG_FLASH_EMPTY_INFO		/* print 'E' for empty sector on flinfo */

/* timeout values are in ticks */
#define CFG_FLASH_ERASE_TOUT	(5*CFG_HZ) /* Timeout for Flash Erase */
#define CFG_FLASH_WRITE_TOUT	(5*CFG_HZ) /* Timeout for Flash Write */

#define CFG_ENV_ADDR        (CFG_FLASH_BASE + 0x1f0000)	/* 1 sector from offset addr flashbase+(1M-64k)bytes: Addr of Environment Sector, 1 sector	boundary*/
#define CFG_ENV_SIZE	    0x10000	/* 1 sector size total:64kbytes, Total Size of Environment Sector */
#define CFG_ENV_SECT_SIZE	0x10000	/* 1 sector size total:64kbytes, Total Size of Environment Sector */

/* u-boot environment(in flash) area setting  */
#define	CFG_ENV_IS_IN_FLASH	1

/* allow to overwrite serial and ethaddr */
#define CONFIG_ENV_OVERWRITE


/*-----------------------------------------------------------------------
 * FLASH and environment organization
 */
#define CFG_MAX_NAND_DEVICE			1
#define CFG_NAND_BASE				0x2C000000  /* shadow=0 */    
#define CFG_NAND_BASE_LIST			{ CFG_NAND_BASE	}

#define CONFIG_MTD_DEBUG
#define CONFIG_MTD_DEBUG_VERBOSE	3
#define NAND_MAX_CHIPS				1
#define NAND_MAX_FLOORS				1
#define NAND_ChipID_UNKNOWN			0x00


// see board/pollux/board.c InitializeGPIO() ==> 강제로 그냥 high로 세팅해 놓았음. 밑의 매크로가 먹지 않는다. 나중에 수정할 것.
//#define POLLUX_NAND_WP 1

#ifndef PHY_BASEADDR_GPIOALV // 이중 include때문
	#define PHY_BASEADDR_GPIOALV			0xC0019000
#endif

#define pRWP	1
#define pSWP	2

#define NAND_WP_OFF()	NAND_WP(0)
#define NAND_WP_ON()	NAND_WP(1)
#define NAND_WP(written)									\
do { 												\
	volatile unsigned int * pGpioBaseAddr = (volatile unsigned int *)PHY_BASEADDR_GPIOALV;	\
	if( 0 == written )	/* protect off */						\
	{											\
		*(pGpioBaseAddr) = (1UL << 0);		/* set GPIO ALIVE Enable  */		\
		*(pGpioBaseAddr+pSWP) = (0UL<<1);	/* set Disable		  */		\
		*(pGpioBaseAddr+pRWP) = (0UL<<1);	/* reset Disable	  */		\
		*(pGpioBaseAddr+pSWP) = (1UL<<1);	/* set Enable		  */		\
		*(pGpioBaseAddr+pSWP) = (0UL<<1);	/* set Disable		  */		\
		*(pGpioBaseAddr+pRWP) = (0UL<<1);	/* reset Disable	  */		\
		*(pGpioBaseAddr) = (0UL << 0);		/* set GPIO ALIVE Disable */		\
	}											\
	else			/* protect on */						\
	{											\
		*(pGpioBaseAddr) = (1UL << 0);		/* set GPIO ALIVE Enable  */		\
		*(pGpioBaseAddr+pSWP) = (0UL<<1);	/* set Disable		  */		\
		*(pGpioBaseAddr+pRWP) = (0UL<<1);	/* reset Disable	  */		\
		*(pGpioBaseAddr+pRWP) = (1UL<<1);	/* reset Enable		  */		\
		*(pGpioBaseAddr+pSWP) = (0UL<<1);	/* set Disable		  */		\
		*(pGpioBaseAddr+pRWP) = (0UL<<1);	/* reset Disable	  */		\
		*(pGpioBaseAddr) = (0UL << 0);		/* set GPIO ALIVE Disable */		\
	}											\
} while(0);


/* ghcstop: tag setting is transfered to kernel */
#define CONFIG_CMDLINE_TAG   		1
#define CONFIG_SETUP_MEMORY_TAGS  	1
#define CONFIG_INITRD_TAG  			1

//#define CONFIG_B2  					1	
//#define CONFIG_AUTO_COMPLETE       	1 

#endif // __CONFIG_H

