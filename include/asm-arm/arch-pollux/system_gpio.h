#ifndef __OEM_SYSTEM_GPIO_H__
#define __OEM_SYSTEM_GPIO_H__


#ifndef CONFIG_GPH_MINI
	///// GROUPA /////
	#define PAD_GPIOA0		PAD_ALT1		// Driver : DISPLAY	, Use : PVD0
	#define PAD_GPIOA1		PAD_ALT1		// Driver : DISPLAY	, Use : PVD1
	#define PAD_GPIOA2		PAD_ALT1		// Driver : DISPLAY	, Use : PVD2
	#define PAD_GPIOA3		PAD_ALT1		// Driver : DISPLAY	, Use : PVD3
	#define PAD_GPIOA4		PAD_ALT1		// Driver : DISPLAY	, Use : PVD4
	#define PAD_GPIOA5		PAD_ALT1		// Driver : DISPLAY	, Use : PVD5
	#define PAD_GPIOA6		PAD_ALT1		// Driver : DISPLAY	, Use : PVD6
	#define PAD_GPIOA7		PAD_ALT1		// Driver : DISPLAY	, Use : PVD7
	#define PAD_GPIOA8		PAD_ALT1		// Driver : UART0	, Use : TX0
	#define PAD_GPIOA9		PAD_ALT1		// Driver : UART1	, Use : nCTS1
	#define PAD_GPIOA10		PAD_ALT1		// Driver : UART1	, Use : nRTS1
	#define PAD_GPIOA11		PAD_ALT1		// Driver : UART1	, Use : nRI1
	#define PAD_GPIOA12		PAD_ALT1		// Driver : UART1	, Use : nDCD1
	#define PAD_GPIOA13		PAD_ALT1		// Driver : UART1	, Use : nDSR1
	#define PAD_GPIOA14		PAD_ALT1		// Driver : UART1	, Use : nDTR1
	#define PAD_GPIOA15		PAD_ALT1		// Driver : UART1	, Use : TX1
	#define PAD_GPIOA16		PAD_ALT1		// Driver : UART1	, Use : RX1
	#define PAD_GPIOA17		PAD_ALT1		// Driver : UART2	, Use : TX2
	#define PAD_GPIOA18		PAD_ALT1		// Driver : UART2	, Use : RX2
	#define PAD_GPIOA19		PAD_ALT1		// Driver : UART3	, Use : TX3
	#define PAD_GPIOA20		PAD_ALT1		// Driver : UART3	, Use : RX3
	#define PAD_GPIOA21		PAD_ALT1		// Driver : I2S		, Use : I2SDOUT
	#define PAD_GPIOA22		PAD_ALT1		// Driver : I2S		, Use : I2SBCLK
	#define PAD_GPIOA23		PAD_ALT1		// Driver : I2S		, Use : I2SDIN
	#define PAD_GPIOA24		PAD_ALT1		// Driver : I2S		, Use : I2SSYNC
	#define PAD_GPIOA25		PAD_ALT1		// Driver : I2S		, Use : I2SMCLK
	#define PAD_GPIOA26		PAD_ALT1		// Driver : I2C		, Use : SCL0
	#define PAD_GPIOA27		PAD_ALT1		// Driver : I2C		, Use : SDA0
	//#define PAD_GPIOA28		PAD_GPIOIN		// Driver : SDMMC	, Use : SD_WP1 //SCL1
	//#define PAD_GPIOA29		PAD_GPIOIN		// Driver : SDMMC	, Use : SD_CD1 //SDA1
	#define PAD_GPIOA28		PAD_ALT1		// Driver : SDMMC	, Use : SD_WP1 //SCL1
	#define PAD_GPIOA29		PAD_ALT1		// Driver : SDMMC	, Use : SD_CD1 //SDA1
	#define PAD_GPIOA30		PAD_ALT1		// Driver : PWM		, Use : PWMOUT0 (LCD BACK)
	#define PAD_GPIOA31		PAD_ALT1		// Driver : PWM		, Use : PWMOUT1 (LED) 

	///// GROUPB /////
	#define PAD_GPIOB0		PAD_ALT1		// Driver : SDMMC		, Use : SDCLK0, SSPCLK2
	#define PAD_GPIOB1		PAD_ALT1		// Driver : SDMMC		, Use : SDCMD0, SSPRXD2
	#define PAD_GPIOB2		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT0, SSPTXD2
	#define PAD_GPIOB3		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT1
	#define PAD_GPIOB4		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT2
	#define PAD_GPIOB5		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT3, SSPFRM2
	#define PAD_GPIOB6		PAD_ALT1		// Driver : SDMMC		, Use : SDCLK1
	#define PAD_GPIOB7		PAD_ALT1		// Driver : SDMMC		, Use : SDCMD1
	#define PAD_GPIOB8		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT1_0
	#define PAD_GPIOB9		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT1_1
	#define PAD_GPIOB10		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT1_2
	#define PAD_GPIOB11		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT1_3
	#define PAD_GPIOB12		PAD_GPIOOUT		// Driver : SPI			, Use : SSPFRM0  ==> 080512: ghcstop_caution, pin is set to GPIO(not ALT1), bug?
	#define PAD_GPIOB13		PAD_GPIOOUT		// Driver : SPI			, Use : SSPCLK0
	#define PAD_GPIOB14		PAD_GPIOIN		// Driver : SPI			, Use : SSPRXD0
	#define PAD_GPIOB15		PAD_GPIOOUT		// Driver : SPI			, Use : SSPTXD0
	#define PAD_GPIOB16		PAD_ALT1		// Driver : DISPLAY		, Use : PVD8
	#define PAD_GPIOB17		PAD_ALT1		// Driver : DISPLAY		, Use : PVD9
	#define PAD_GPIOB18		PAD_ALT1		// Driver : DISPLAY	, Use : PVD10
	#define PAD_GPIOB19		PAD_ALT1		// Driver : DISPLAY	, Use : PVD11
	#define PAD_GPIOB20		PAD_ALT1		// Driver : DISPLAY	, Use : PVD12
	#define PAD_GPIOB21		PAD_ALT1		// Driver : DISPLAY	, Use : PVD13
	#define PAD_GPIOB22		PAD_ALT1		// Driver : DISPLAY	, Use : PVD14
	#define PAD_GPIOB23		PAD_ALT1		// Driver : DISPLAY	, Use : PVD15
	#define PAD_GPIOB24		PAD_ALT1		// Driver : DISPLAY	, Use : PVD16
	#define PAD_GPIOB25		PAD_ALT1		// Driver : DISPLAY	, Use : PVD17
	#define PAD_GPIOB26		PAD_ALT1		// Driver : DISPLAY	, Use : PVD18
	#define PAD_GPIOB27		PAD_ALT1		// Driver : DISPLAY	, Use : PVD19
	#define PAD_GPIOB28		PAD_ALT1		// Driver : DISPLAY	, Use : PVD20
	#define PAD_GPIOB29		PAD_ALT1		// Driver : DISPLAY	, Use : PVD21
	#define PAD_GPIOB30		PAD_ALT1		// Driver : DISPLAY	, Use : PVD22
	#define PAD_GPIOB31		PAD_ALT1		// Driver : DISPLAY	, Use : PVD23

	///// GROUPC /////
	#define PAD_GPIOC0		PAD_GPIOIN		// Driver : TOUCH		, Use : PENDOWN_DECT //nSCS7
	#define PAD_GPIOC1		PAD_ALT1		// Driver : DOT-MATRIX	, Use : nSCS8
	#define PAD_GPIOC2		PAD_GPIOIN		// Driver : XXXXX		, Use : XXXX 	//nSCS9
	#define PAD_GPIOC3		PAD_GPIOOUT		// Driver : TOUCH		, Use : YMON    //SSPFRM1
	#define PAD_GPIOC4		PAD_GPIOOUT		// Driver : TOUCH		, Use : NYPON  //SSPCLK1
	#define PAD_GPIOC5		PAD_GPIOOUT		// Driver : TOUCH		, Use : XMON   //SSPRX1
	#define PAD_GPIOC6		PAD_GPIOOUT		// Driver : TOUCH		, Use : NXPON  //SSPTX1
	#define PAD_GPIOC7		PAD_GPIOIN      // Driver : SOUND		, Use : EARPHONE_DECT //PWMOUT2
	#define PAD_GPIOC8		PAD_GPIOIN      // Driver : BUTTON		, Use : UP-BUTTON //SA19
	#define PAD_GPIOC9		PAD_GPIOIN      // Driver : BUTTON		, Use : DWON-BUTTON //SA20
	#define PAD_GPIOC10		PAD_GPIOIN      // Driver : BUTTON		, Use : LEFT-BUTTON //SA21
	#define PAD_GPIOC11		PAD_GPIOIN      // Driver : BUTTON		, Use : RIGHT-BUTTON //SA22
	#define PAD_GPIOC12		PAD_GPIOIN      // Driver : BUTTON		, Use : ENTER-BUTTON //SA23
	#define PAD_GPIOC13		PAD_GPIOIN      // Driver : ETHER		, Use : ETHER_IRQ //SA24
	#define PAD_GPIOC14		PAD_GPIOIN      // Driver : IDE			, Use : IDE_IRQ //SA25
	#define PAD_GPIOC15		PAD_ALT1		// Driver : IDE			, Use : IDE_nSCS2, nICE1
	#define PAD_GPIOC16		PAD_ALT1		// Driver : STATIC		, Use : nSCS3, nICE2
	#define PAD_GPIOC17		PAD_ALT1		// Driver : STATIC		, Use : nSCS4
	#define PAD_GPIOC18		PAD_GPIOIN		// Driver : SDMMC		, Use : SD0_CD  //nSCS5
	#define PAD_GPIOC19		PAD_GPIOIN		// Driver : SDMMC		, Use : SD0_WP  //
	#define PAD_GPIOC20		PAD_ALT1		// Driver : POWER		, Use : VDDPWRTOGGLE

	///// GROUPALV /////
	#define PAD_GPIOALV0	PAD_GPIOOUT		// Driver : NAND		, Use : NAND_SLC_WP
	#define PAD_GPIOALV1	PAD_GPIOOUT		// Driver : NAND		, Use : NAND_MLC_WP
	#define PAD_GPIOALV2	PAD_GPIOOUT		// Driver : DAC			, Use : DAC_RES
	#define PAD_GPIOALV3	PAD_GPIOOUT		// Driver : DISPLAY		, Use : VOUT_DISPLAY
	#define PAD_GPIOALV4	PAD_GPIOOUT		// Driver : DISPLAY		, Use : BL_ENB
	#define PAD_GPIOALV5	PAD_GPIOOUT		// Driver : IDE			, Use : IDE_RST
	#define PAD_GPIOALV6	PAD_GPIOOUT		// Driver : TOUCH		, Use : PENDOWN_CON


	// Owner : TOUCH
	#define OEM_GPIO_TSMY_EN              			(32*2+3)
	#define OEM_GPIO_TSMY_EN_AL           			OUT_HIGH

	#define OEM_GPIO_TSPY_EN              			(32*2+4)
	#define OEM_GPIO_TSPY_EN_AL           			OUT_LOW

	#define OEM_GPIO_TSMX_EN              			(32*2+5)
	#define OEM_GPIO_TSMX_EN_AL           			OUT_HIGH

	#define OEM_GPIO_TSPX_EN              			(32*2+6)
	#define OEM_GPIO_TSPX_EN_AL           			OUT_LOW

	#define OEM_GPIO_PENDOWN_CON          			(32*3+6)
	#define OEM_GPIO_PENDOWN_CON_AL       			OUT_HIGH

	#define OEM_GPIO_PENDOWN_DET          			(32*2+0)
	#define OEM_GPIO_PENDOWN_DET_ACTIVE   			LOW_LEVEL
	#define OEM_GPIO_PENDOWN_DET_INACTIVE 			HIGH_LEVEL
	#define OEM_GPIO_PENDOWN_DET_IRQ      			IRQ_VIR22
	#define OEM_GPIO_PENDOWN_DET_AL       			OUT_LOW

	// Owner : DISPLAY
	#define OEM_GPIO_LCD_ENB              			(32*3+3)
	#define OEM_GPIO_LCD_ENB_AL           			OUT_HIGH

	#define OEM_GPIO_BL_ENB               			(32*3+4)
	#define OEM_GPIO_BL_ENB_AL            			OUT_HIGH

	// Owner : SD_CARD
	#define OEM_GPIO_SD_CD                			(32*2+18)
	#define OEM_GPIO_SD_CD_ACTIVE         			LOW_LEVEL
	#define OEM_GPIO_SD_CD_INACTIVE       			HIGH_LEVEL
	#define OEM_GPIO_SD_CD_AL             			OUT_LOW

	#define OEM_GPIO_SD_WP                			(32*2+19)
	#define OEM_GPIO_SD_WP_ACTIVE         			LOW_LEVEL
	#define OEM_GPIO_SD_WP_INACTIVE       			HIGH_LEVEL
	#define OEM_GPIO_SD_WP_AL             			OUT_LOW

	// Owner : AUDIO
	#define OEM_GPIO_AUDIO_RST           			(32*3+2)
	#define OEM_GPIO_AUDIO_RST_AL					OUT_LOW

	#define OEM_GPIO_EARPHONE_DECT                  (32*2+7)
	#define OEM_GPIO_EARPHONE_DECT_ACTIVE           LOW_LEVEL		
	#define OEM_GPIO_EARPHONE_DECT_INACTIVE         HIGH_LEVEL
	#define OEM_GPIO_EARPHONE_DECT_AL               OUT_LOW

	// Owner : IDE_PIO
	#define OEM_GPIO_IDE_RST              			(32*3+5)
	#define OEM_GPIO_IDE_RST_AL           			OUT_LOW

	// Owner : BUTTONS
	#define OEM_GPIO_KEY_UP            				(32*2+8)
	#define OEM_GPIO_KEY_UP_ACTIVE     				FALLING_EDGE
	#define OEM_GPIO_KEY_UP_INACTIVE   				RISING_EDGE
	#define OEM_GPIO_KEY_UP_IRQ        				IRQ_VIR27
	#define OEM_GPIO_KEY_UP_AL         				OUT_LOW

	#define OEM_GPIO_KEY_DOWN            			(32*2+9)
	#define OEM_GPIO_KEY_DOWN_ACTIVE     			FALLING_EDGE
	#define OEM_GPIO_KEY_DOWN_INACTIVE   			RISING_EDGE
	#define OEM_GPIO_KEY_DOWN_IRQ        			IRQ_VIR27
	#define OEM_GPIO_KEY_DOWN_AL         			OUT_LOW

	#define OEM_GPIO_KEY_LEFT             			(32*2+10)
	#define OEM_GPIO_KEY_LEFT_ACTIVE      			FALLING_EDGE
	#define OEM_GPIO_KEY_LEFT_INACTIVE    			RISING_EDGE
	#define OEM_GPIO_KEY_LEFT_IRQ         			IRQ_VIR27
	#define OEM_GPIO_KEY_LEFT_AL          			OUT_LOW

	#define OEM_GPIO_KEY_RIGHT             			(32*2+11)
	#define OEM_GPIO_KEY_RIGHT_ACTIVE      			FALLING_EDGE
	#define OEM_GPIO_KEY_RIGHT_INACTIVE    			RISING_EDGE
	#define OEM_GPIO_KEY_RIGHT_IRQ         			IRQ_VIR27
	#define OEM_GPIO_KEY_RIGHT_AL          			OUT_LOW

	#define OEM_GPIO_KEY_ENTER              		(32*2+12)
	#define OEM_GPIO_KEY_ENTER_ACTIVE       		FALLING_EDGE
	#define OEM_GPIO_KEY_ENTER_INACTIVE     		RISING_EDGE
	#define OEM_GPIO_KEY_ENTER_IRQ          		IRQ_VIR27
	#define OEM_GPIO_KEY_ENTER_AL           		OUT_LOW

	// Owner : NAND
	#define OEM_GPIO_NAND_SLCWP              		(32*3+0)
	#define OEM_GPIO_NAND_SLCWP_AL           		OUT_LOW

	#define OEM_GPIO_NAND_MLCWP              		(32*3+1)
	#define OEM_GPIO_NAND_MLCWP_AL           		OUT_LOW


#else

//////////////////////////////////////////////////////////////////////////
	///// GROUPA /////
	#define PAD_GPIOA0		PAD_ALT1		// Driver : DISPLAY	, Use : PVD0
	#define PAD_GPIOA1		PAD_ALT1		// Driver : DISPLAY	, Use : PVD1
	#define PAD_GPIOA2		PAD_ALT1		// Driver : DISPLAY	, Use : PVD2
	#define PAD_GPIOA3		PAD_ALT1		// Driver : DISPLAY	, Use : PVD3
	#define PAD_GPIOA4		PAD_ALT1		// Driver : DISPLAY	, Use : PVD4
	#define PAD_GPIOA5		PAD_ALT1		// Driver : DISPLAY	, Use : PVD5
	#define PAD_GPIOA6		PAD_ALT1		// Driver : DISPLAY	, Use : PVD6
	#define PAD_GPIOA7		PAD_ALT1		// Driver : DISPLAY	, Use : PVD7
	#define PAD_GPIOA8		PAD_ALT1		// Driver : UART0	, Use : TX0
	//#define PAD_GPIOA9		PAD_GPIOIN		// Driver : SOUND	, Use : TV/SPK (NOT)
	#define PAD_GPIOA9		PAD_GPIOOUT		// Driver : SOUND	, Use : TV/SPK (NOT)
	#define PAD_GPIOA10		PAD_GPIOIN		// Driver : POWER	, Use : HOLD_KEY
	#define PAD_GPIOA11		PAD_GPIOIN		// Driver : LCD	    , Use : VSYNC_CHK
	#define PAD_GPIOA12		PAD_ALT1		// Driver : UART1	, Use : nDCD1
	#define PAD_GPIOA13		PAD_ALT1		// Driver : UART1	, Use : nDSR1
	#define PAD_GPIOA14		PAD_ALT1		// Driver : UART1	, Use : nDTR1
	#define PAD_GPIOA15		PAD_ALT1		// Driver : UART1	, Use : TX1
	#define PAD_GPIOA16		PAD_ALT1		// Driver : UART1	, Use : RX1
	#define PAD_GPIOA17		PAD_GPIOIN		// Driver : TOUCH	, Use : PENDOWN DECT

#ifndef CONFIG_FINAL_BOARD   
	#define PAD_GPIOA18		PAD_GPIOOUT		// Driver : POWER	, Use : MAIN_POWER_CTRL
#else	/* final_board */
	#define PAD_GPIOA18		PAD_GPIOIN		// Driver : POWER	, Use : MAIN_POWER_CTRL
#endif

	#define PAD_GPIOA19		PAD_GPIOIN		// Driver : SDMMC	, Use : SD_CD
	#define PAD_GPIOA20		PAD_GPIOIN		// Driver : SDMMC	, Use : SD_WP
	#define PAD_GPIOA21		PAD_ALT1		// Driver : I2S		, Use : I2SDOUT
	#define PAD_GPIOA22		PAD_ALT1		// Driver : I2S		, Use : I2SBCLK
	#define PAD_GPIOA23		PAD_ALT1		// Driver : I2S		, Use : I2SDIN
	#define PAD_GPIOA24		PAD_ALT1		// Driver : I2S		, Use : I2SSYNC
	#define PAD_GPIOA25		PAD_ALT1		// Driver : I2S		, Use : I2SMCLK
	#define PAD_GPIOA26		PAD_ALT1		// Driver : I2C		, Use : SCL0
	#define PAD_GPIOA27		PAD_ALT1		// Driver : I2C		, Use : SDA0
	#define PAD_GPIOA28		PAD_ALT1		// Driver : EXT_RTC	, Use : SCL1 
	#define PAD_GPIOA29		PAD_ALT1		// Driver : EXT_RTC	, Use : SDA1 
    
#ifndef CONFIG_GPH_F300	
	#define PAD_GPIOA30		PAD_GPIOIN		// Driver : NOT		, Use : 
#else	
    #define PAD_GPIOA30		PAD_ALT1		// Driver : PWM		, Use : PWMOUT0 (LCD BACK)
#endif	
	
	#define PAD_GPIOA31		PAD_GPIOIN		// Driver : NOT		, Use :  

	///// GROUPB /////ioo
	#define PAD_GPIOB0		PAD_ALT1		// Driver : SDMMC		, Use : SDCLK0, SSPCLK2
	#define PAD_GPIOB1		PAD_ALT1		// Driver : SDMMC		, Use : SDCMD0, SSPRXD2
	#define PAD_GPIOB2		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT0, SSPTXD2
	#define PAD_GPIOB3		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT1
	#define PAD_GPIOB4		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT2
	#define PAD_GPIOB5		PAD_ALT1		// Driver : SDMMC		, Use : SDDAT3, SSPFRM2
	#define PAD_GPIOB6		PAD_GPIOIN		// Driver : GPIO		, Use : R_SELECT
	#define PAD_GPIOB7		PAD_GPIOIN		// Driver : GPIO		, Use : L_SELECT
	#define PAD_GPIOB8		PAD_GPIOIN		// Driver : GPIO		, Use : SELECT
	#define PAD_GPIOB9		PAD_GPIOIN		// Driver : GPIO		, Use : HOME
	#define PAD_GPIOB10		PAD_GPIOIN		// Driver : GPIO		, Use : VOLUME_UP
	#define PAD_GPIOB11		PAD_GPIOIN		// Driver : GPIO		, Use : VOLUME_DN
	#define PAD_GPIOB12		PAD_GPIOOUT		// Driver : USB         , Use : USB5V_CTRL
	#define PAD_GPIOB13		PAD_GPIOIN		// Driver : 
	#define PAD_GPIOB14		PAD_GPIOOUT		// Driver : DISPLAY		, Use : LCD_ARVDD_CTRL
	#define PAD_GPIOB15		PAD_GPIOOUT		// Driver : DISPLAY		, Use : LCD_RST
	//#define PAD_GPIOB15		PAD_GPIOOUT_PULLUP	// Driver : DISPLAY		, Use : LCD_RSTs
	#define PAD_GPIOB16		PAD_ALT1		// Driver : DISPLAY		, Use : PVD8
	#define PAD_GPIOB17		PAD_ALT1		// Driver : DISPLAY		, Use : PVD9
	#define PAD_GPIOB18		PAD_ALT1		// Driver : DISPLAY	, Use : PVD10
	#define PAD_GPIOB19		PAD_ALT1		// Driver : DISPLAY	, Use : PVD11
	#define PAD_GPIOB20		PAD_ALT1		// Driver : DISPLAY	, Use : PVD12
	#define PAD_GPIOB21		PAD_ALT1		// Driver : DISPLAY	, Use : PVD13
	#define PAD_GPIOB22		PAD_ALT1		// Driver : DISPLAY	, Use : PVD14
	#define PAD_GPIOB23		PAD_ALT1		// Driver : DISPLAY	, Use : PVD15
	#define PAD_GPIOB24		PAD_ALT1		// Driver : DISPLAY	, Use : PVD16
	#define PAD_GPIOB25		PAD_ALT1		// Driver : DISPLAY	, Use : PVD17
	#define PAD_GPIOB26		PAD_GPIOOUT		// Driver : DISPLAY	, Use : LCD_RW
	#define PAD_GPIOB27		PAD_GPIOOUT		// Driver : DISPLAY	, Use : LCD_CS
#ifndef CONFIG_GPH_F300		
	#define PAD_GPIOB28		PAD_GPIOIN		// Driver : DISPLAY	, Use : LCD_SDO
#else
    #define PAD_GPIOB28		PAD_GPIOOUT		// Driver : DISPLAY	, Use : LCD_SCL
#endif	
	
	#define PAD_GPIOB29		PAD_GPIOOUT		// Driver : DISPLAY	, Use : LCD_SDI
	#define PAD_GPIOB30		PAD_GPIOIN		// Driver : EXT_RTC	, Use : ALARM
	#define PAD_GPIOB31		PAD_GPIOIN		// Driver : POWER	, Use : POWER_CHK 

	///// GROUPC /////
	#define PAD_GPIOC0		PAD_GPIOIN		// Driver : GPIO		, Use : LEFT
	#define PAD_GPIOC1		PAD_GPIOIN		// Driver : GPIO		, Use : RIGHT
	#define PAD_GPIOC2		PAD_GPIOIN		// Driver : GPIO		, Use : UP
	#define PAD_GPIOC3		PAD_GPIOIN		// Driver : GPIO		, Use : DN
	#define PAD_GPIOC4		PAD_GPIOIN		// Driver : GPIO		, Use : A
	#define PAD_GPIOC5		PAD_GPIOIN		// Driver : GPIO		, Use : B
	#define PAD_GPIOC6		PAD_GPIOIN		// Driver : GPIO		, Use : X
	#define PAD_GPIOC7		PAD_GPIOIN      // Driver : GPIO		, Use : Y
	#define PAD_GPIOC8		PAD_ALT1        // Driver : STATIC		, Use : SA19
	#define PAD_GPIOC9		PAD_ALT1        // Driver : STATIC		, Use : SA20
	#define PAD_GPIOC10		PAD_ALT1        // Driver : STATIC		, Use : SA21
	#define PAD_GPIOC11		PAD_GPIOOUT       // Driver : TOUCH		, Use : YMON
	#define PAD_GPIOC12		PAD_GPIOOUT       // Driver : TOUCH		, Use : NYPON
	#define PAD_GPIOC13		PAD_GPIOOUT       // Driver : TOUCH		, Use : XMON
	#define PAD_GPIOC14		PAD_GPIOOUT       // Driver : TOUCH		, Use : NXPON
	#define PAD_GPIOC15		PAD_GPIOIN		// Driver : USB			, Use : USB_INSERT_CHECK
	#define PAD_GPIOC16		PAD_GPIOOUT		// Driver : LED			, Use : LED_DEBUG
	#define PAD_GPIOC17		PAD_GPIOIN		// Driver : VERSION[1]	, Use : BOARD_VERSION(MSB)
	#define PAD_GPIOC18		PAD_GPIOIN		// Driver : VERSION[0]	, Use : BOARD_VERSION(LSB)
	#define PAD_GPIOC19		PAD_GPIOOUT		// Driver : TOUCH		, Use : PENDOWN_CON
	#define PAD_GPIOC20		PAD_ALT1		// Driver : POWER		, Use : VDDPWRTOGGLE
	#define PAD_GPIOALV0	PAD_GPIOOUT		// Driver : 			, Use : 
	#define PAD_GPIOALV1	PAD_GPIOOUT		// Driver : NAND		, Use : NAND_MLC_WP
	#define PAD_GPIOALV2	PAD_GPIOOUT		// Driver : DAC(SOUND)	, Use : DAC_RES
	#define PAD_GPIOALV3	PAD_GPIOIN		// Driver : NOT			, Use : 
	#define PAD_GPIOALV4	PAD_GPIOIN		// Driver : NOT			, Use : 
	#define PAD_GPIOALV5	PAD_GPIOIN		// Driver : NOT			, Use : 
	#define PAD_GPIOALV6	PAD_GPIOOUT		// Driver : NOT			, Use : 

	// Owner : DISPLAY
	#define OEM_GPIO_LCD_ARVDD              		(32*1+14)
	#define OEM_GPIO_LCD_ENB              			(32*1+15)
	#define OEM_GPIO_LCD_RW_NRD						(32*1+26)
	#define OEM_GPIO_LCD_CS							(32*1+27)

#ifndef CONFIG_GPH_F300	
	#define OEM_GPIO_LCD_SDO						(32*1+28)
#else	
    #define OEM_GPIO_LCD_SCL						(32*1+28)
#endif
	#define OEM_GPIO_LCD_SDI						(32*1+29)

    #define OEM_GPIO_MPWR_CTRL						(32*0+18)
    
	#define OEM_GPIO_TEST_KEY						(32*1+6)

    #define OEM_GPIO_USB5V_CTRL						(32*1+12)
    
    #define OEM_GPIO_BD_NUM_LSB                     (32*2+18)
    
    #define OEM_GPIO_BD_NUM_MSB                     (32*2+17)

    #define OEM_GPIO_SPK_ENB                        (32*0+9)
        
#endif  /* CONFIG_GPH_MINI */





#endif  //__OEM_SYSTEM_GPIO_H__
