//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//

#include <common.h>
#include <asm/arch/mes_pollux.h>
#include <command.h>
#include <asm/processor.h>
#include <asm/io.h>


#include "pollux_nand_config.h"


extern short BCH_AlphaToTable[8192];
extern short BCH_IndexOfTable[8192];

unsigned int gBlockOffset;
unsigned int gPageOffset;


#if (NAND_LARGE_BLOCK==CTRUE)
	//#define ECC_SIZE        2*8       // 4k page?
	#define ECC_SIZE        2*4         // ghcstop fix for 2k page
#else
	#define ECC_SIZE        2
#endif


#define NAND_WRENCDONE		0x1
#define NAND_RDDECDONE		0x2
#define NAND_RDCHECKERR		0x4


volatile 	PNANDECC		pECC;
volatile 	PNANDOFFSET		pNANDOffset;

volatile unsigned int *p_WP;


static unsigned int		ggnCycle = NAND_ADDRESS_TYPE;

void    reset_ecc( void )
{
    const U32   ECCRST_MASK = 1UL << 11;
    const U32   IRQPEND_MASK = 1UL << 15;

    register U32 regvalue;
 
    regvalue = pECC->reg_NFCONTROL;

    regvalue &= ~(IRQPEND_MASK | ECCRST_MASK );
    regvalue |= ECCRST_MASK;

    pECC->reg_NFCONTROL = regvalue;
}


void ReadOddSyndrome(int  *s)
{
    int syn_val13, syn_val57;
    
    syn_val13 = pECC->reg_NFSYNDRONE31;
    syn_val57 = pECC->reg_NFSYNDRONE75;

    s[0] = syn_val13 & 0x1fff;
    s[2] = (syn_val13>>13) & 0x1fff;
    s[4] = syn_val57 & 0x1fff;
    s[6] = (syn_val57>>13) & 0x1fff;
}

//------------------------------------------------------------------------------
int		MES_NAND_GetErrorLocation(  int  *s, int *pLocation )
{
	register int i, j, elp_sum ;
	int count;
	int r;				// Iteration steps
	int Delta; 			// Discrepancy value
	int elp[8+1][8+2]; 	// Error locator polynomial (ELP)
	int L[8+1];			// Degree of ELP 
	int B[8+1][8+2];	// Scratch polynomial
	int reg[8+1];		// Register state


	// Even syndrome = (Odd syndrome) ** 2
	for( i=1,j=0 ; i<8 ; i+=2, j++ )
	{ 	
		if( s[j] == 0 )		s[i] = 0;
		else				s[i] = BCH_AlphaToTable[(2 * BCH_IndexOfTable[s[j]]) % 8191];
	}
	
	// Initialization of elp, B and register
	for( i=0 ; i<=8 ; i++ )
	{	
		L[i] = 0 ;
		for( j=0 ; j<=8 ; j++ )
		{	
			elp[i][j] = 0 ;
			B[i][j] = 0 ;
		}
	}

	for( i=0 ; i<=4 ; i++ )
	{	
		reg[i] = 0 ;
	}
	
	elp[1][0] = 1 ;
	elp[1][1] = s[0] ;

	L[1] = 1 ;
	if( s[0] != 0 )
		B[1][0] = BCH_AlphaToTable[(8191 - BCH_IndexOfTable[s[0]]) % 8191];
	else
		B[1][0] = 0;
	
	for( r=3 ; r<=8-1 ; r=r+2 )
	{	
		// Compute discrepancy
		Delta = s[r-1] ;
		for( i=1 ; i<=L[r-2] ; i++ )
		{	
			if( (s[r-i-1] != 0) && (elp[r-2][i] != 0) )
				Delta ^= BCH_AlphaToTable[(BCH_IndexOfTable[s[r-i-1]] + BCH_IndexOfTable[elp[r-2][i]]) % 8191];
		}
		
		if( Delta == 0 )
		{	
			L[r] = L[r-2] ;
			for( i=0 ; i<=L[r-2] ; i++ )
			{	
				elp[r][i] = elp[r-2][i];
				B[r][i+2] = B[r-2][i] ;
			}
		}
		else
		{	
			// Form new error locator polynomial
			for( i=0 ; i<=L[r-2] ; i++ )
			{	
				elp[r][i] = elp[r-2][i] ;
			}

			for( i=0 ; i<=L[r-2] ; i++ )
			{	
				if( B[r-2][i] != 0 )
					elp[r][i+2] ^= BCH_AlphaToTable[(BCH_IndexOfTable[Delta] + BCH_IndexOfTable[B[r-2][i]]) % 8191];
			}
			
			// Form new scratch polynomial and register length
			if( 2 * L[r-2] >= r )
			{	
				L[r] = L[r-2] ;
				for( i=0 ; i<=L[r-2] ; i++ )
				{	
					B[r][i+2] = B[r-2][i];
				}
			}
			else
			{	
				L[r] = r - L[r-2];
				for( i=0 ; i<=L[r-2] ; i++ )
				{	
					if( elp[r-2][i] != 0 )
						B[r][i] = BCH_AlphaToTable[(BCH_IndexOfTable[elp[r-2][i]] + 8191 - BCH_IndexOfTable[Delta]) % 8191];
					else
						B[r][i] = 0;
				}
			}
		}
	}
	
	if( L[8-1] > 4 )
	{	
		//return L[8-1];
		return -1;
	}
	else
	{	
		// Chien's search to find roots of the error location polynomial
		// Ref: L&C pp.216, Fig.6.1
		for( i=1 ; i<=L[8-1] ; i++ )
			reg[i] = elp[8-1][i];
		
		count = 0;
		for( i=1 ; i<=8191 ; i++ )
		{ 	
			elp_sum = 1;
			for( j=1 ; j<=L[8-1] ; j++ )
			{	
				if( reg[j] != 0 )
				{ 	
					reg[j] = BCH_AlphaToTable[(BCH_IndexOfTable[reg[j]] + j) % 8191] ;
					elp_sum ^= reg[j] ;
				}
			}
			
			if( !elp_sum )		// store root and error location number indices
			{ 	
				// Convert error location from systematic form to storage form 
				pLocation[count] = 8191 - i;
				
				if (pLocation[count] >= 52)
				{	
					// Data Bit Error
					pLocation[count] = pLocation[count] - 52;
					pLocation[count] = 4095 - pLocation[count];
				}
				else
				{
					// ECC Error
					pLocation[count] = pLocation[count] + 4096;
				}

				if( pLocation[count] < 0 )	return -1;

				count++;
			}
		}
		
		if( count == L[8-1] )	// Number of roots = degree of elp hence <= 4 errors
		{	
			return L[8-1];
		}
		else	// Number of roots != degree of ELP => >4 errors and cannot solve
		{	
			return -1;
		}
	}
}


void *fmd_Init(void)
{
	// Caller should have specified NAND controller address.
	//

	gprintf("1\n");
	
	pNANDOffset = (PNANDOFFSET)CFG_NAND_BASE;
	pECC	    = (PNANDECC)PHY_BASEADDR_NANDECC;

	CLEAR_RnB();
	NF_CMD(CMD_RESET);		// Send reset command.

	gprintf("reset done\n");
	

	return((void *)pNANDOffset);
}



CBOOL fmd_ReadSector(
	unsigned long  startSectorAddr, // page offset, 즉 block이랑 상관없이 page단위의 움직임이다.
	unsigned char *pSectorBuff,     // buffer(sector수만큼 ==> page수만큼)
	PSectorInfo    pSectorInfoBuff, // spare area buffer, 위의 buffer와 or이다....즉, spare만도 읽을 수 있음.
	unsigned int   dwNumSectors)    // sector 갯수.
{
	unsigned long SectorAddr = (unsigned long)startSectorAddr;
		
	unsigned int dwECC[ECC_SIZE]; // small일 경우는 2, large일 경우는 2*8 = 16개
	unsigned int *pdwSectorAddr;
	unsigned int dw_ErrOccur = 0;
	int i, j;
	unsigned int loop;
	
	gprintf("fmd_ReadSector %d\n",SectorAddr);


	#if (NAND_LARGE_BLOCK==CTRUE)
		unsigned int	dwByteCnt= NAND_PAGE_SIZE;
	#else
		unsigned int	dwByteCnt= 512;
	#endif


	// Send reset command.

	CLEAR_RnB();

	while (dwNumSectors--) // sector수만큼
	{
		unsigned long blockPage	= SectorAddr; // sector 시작을 blockpage로 할당? 왜?, SectorAddr은 밑에서 증가시킨다. 즉, 다음 page를 읽을 수 있도록
		unsigned long DataOffset= 0, SpareOffset= 0;

		#if (NAND_LARGE_BLOCK==CTRUE)
		{
			SpareOffset	= NAND_PAGE_SIZE; // large block의 경우는 spare영역을 읽는 명령이 따로 존재하지 않는다. 그냥 데이터와 동일하게 친다.
		}
		#endif
				
		#if (NAND_ECC_CHK==CTRUE) // 먼저 SPARE를 읽어서 해당되는 ECC값을 읽어와야 한다. 
			if (pSectorBuff)
			{
				#if (NAND_LARGE_BLOCK==CTRUE)
				{
					// Spare만 읽은 경우
					CLEAR_RnB();
					gprintf("Spareoffset = %d\n", SpareOffset);
					NF_SETREAD(blockPage, SpareOffset, ggnCycle, CTRUE);  // sector 시작의 spare area를 읽는다.
				}
				#else
				{
					CLEAR_RnB();
					NF_SETREADSPARE( blockPage, ggnCycle );
				}
				#endif
				

				#if (NAND_LARGE_BLOCK==CFALSE)
					CLEAR_RnB();
				#endif				
				// Read page/sector information.
				for ( i=0; i<2; i++) // 8 bytes
				{
					unsigned int ch;
		
					ch= NF_RDDATA32(); 
					gprintf("%d, ch = 0x%08x\n", i, ch);
				}
				
				// Read the ECC info
				for( i=0; i<ECC_SIZE; i++)
				{
					dwECC[i] = NF_RDDATA32(); // small은 4bytes짜리 2개, large는 16개
					//  Read out the ECC bits saved
					gprintf("dwECC[%d] = 0x%08x\n", i, dwECC[i]);
					
				}
				#if (NAND_LARGE_BLOCK==CFALSE)
				//	CHECK_RnB();
					NF_WAITB()
				#endif				
			}
		#endif			

		for(loop = 0 ; loop < dwByteCnt/512 ; loop++) // large block의 경우는 dwByteCnt는 page(2k 등등)크기가 되고, small일 경우는 512이다.
		{
			if (pSectorBuff)
			{
				//reset_ecc();
				
			#if (NAND_ECC_CHK==CTRUE)	
				printf("R %d, pECC->reg_NFORGECCL = 0x%x\n", loop*512, dwECC[((loop * 2) + 0)]);
				printf("R %d, pECC->reg_NFORGECCH = 0x%x\n", loop*512, dwECC[((loop * 2) + 1)]);

				pECC->reg_NFORGECCL	= dwECC[((loop * 2) + 0)]; // ECC를 2개 단위로 access하고, 그 중 하위 한개는 low에 
				pECC->reg_NFORGECCH	= dwECC[((loop * 2) + 1)]; // 그 중 상위 한개는 high에 할당한다.

			#endif
						
				CLEAR_RnB();
				NF_SETREAD(blockPage, DataOffset + loop * 512, ggnCycle, CTRUE); // data offset서부터 512개씩 읽어오도록 번지지정
	
				//  Handle unaligned buffer pointer
				if( ((unsigned int) pSectorBuff) & 0x3 ) // 데이터가 들어가야할 4bytes로 정렬되어 있지 않을 경우는
				{
					for ( i=0; i<512; i++) // 1bytes씩 읽는다.
					{
						unsigned char ch;
	
						ch= NF_RDDATA();
						*((unsigned char *)pSectorBuff+i)= ch;
					}
				}
				else // 4bytes로 정렬되어 있을 경우는 4bytes씩 읽는다.
				{
					for ( i=0; i<512/4; i++)
					{
						unsigned int ch;
	
						ch= NF_RDDATA32();
						*((unsigned int *)pSectorBuff+i)= ch;
						//gprintf("ch = %08x\n", ch);
					}
				}
				
			#if (NAND_ECC_CHK==CTRUE)	

				while(0 == (pECC->reg_NFECCSTATUS & (NAND_RDDECDONE /*| NAND_RDCHECKERR*/))); // read done이 되었을 때
				
				if( pECC->reg_NFECCSTATUS & NAND_RDCHECKERR ) // error check 했는데, error가 있을 경우는
				{
				    int conv_location;
					int dSyndrome[8];
				   	int	ErrCnt, ErrPos[4];
					
					gprintf("err %d, pECC->reg_NFORGECCL = 0x%x\n", loop*512, pECC->reg_NFORGECCL);
					gprintf("err %d, pECC->reg_NFORGECCH = 0x%x\n", loop*512, pECC->reg_NFORGECCH);

					gprintf("RD ERR!!! pECC->reg_NFECCSTATUS = 0x%x!!!!\n",pECC->reg_NFECCSTATUS);
					// ERROR 발생 여기에 Error Correction code를 넣어야 한다. 

			        ReadOddSyndrome(dSyndrome);
       				pdwSectorAddr = (unsigned int *)pSectorBuff;

					ErrCnt = MES_NAND_GetErrorLocation( dSyndrome, ErrPos );

					if( ErrCnt < 0 )
					{
						gprintf("%d block, Failed to fix error bit, Break!\n", blockPage);
						dw_ErrOccur = 1;	
						//return CFALSE;
					}
					
					gprintf("%d error found and fixed.,\n", ErrCnt);
					for( j=0 ; j<ErrCnt ; j++ )
					{
						pdwSectorAddr[ErrPos[j]/32] ^= 1<<(ErrPos[j]%32);
						//  pSectorBuff[ErrPos[j]/32] ^= 1<<(ErrPos[j]%32);
						gprintf("Modified to ... = 0x%x \n",pdwSectorAddr[ErrPos[j]/32]);
					}		        

				}

			#endif			
				pSectorBuff += 512; // 512 bytes 만큼 read buffer 증가.

			}

		}
	
		#if (NAND_LARGE_BLOCK==CTRUE)
		{
			if( pSectorBuff ) // Data를 읽고, Spare도 추가적으로 읽은 경우 ==> data읽고, spare도 읽을 경우
			{
				NF_SETREADSPARE_SEQ( SpareOffset );
			}
			else // Spare만 읽은 경우 ==> spare만 읽을 경우
			{
				CLEAR_RnB();
				NF_SETREAD(blockPage, SpareOffset, ggnCycle, CTRUE);
			}
		}
		#else // small block의 경우는
		{
			if (pSectorBuff) // data만 읽었을때는 할필요 없고....
			{
			}
			else // spare만 읽을 경우는 read spare command를 내린다.
			{
				CLEAR_RnB();
				NF_SETREADSPARE( blockPage, ggnCycle );
			}
		}
		#endif
		
		if (pSectorInfoBuff) // spare를 읽을 버퍼가 있을 경우
		{
			// Read page/sector information.
			for( i=0; i<8; i++)
			{
				unsigned char ch;

				ch= NF_RDDATA();
				*((unsigned char *)pSectorInfoBuff+i)= ch;
			}

			pSectorInfoBuff++;
		}

		++SectorAddr;
	}

	if(dw_ErrOccur != 0)
		return CFALSE;
		
	return(CTRUE);
}



// block number
CBOOL fmd_EraseBlock(unsigned int blockID) 
{
	unsigned char Status;

	gprintf("fmd_EraseBlock %d\n", blockID);

	CLEAR_RnB();
	NF_SETERASE( blockID, ggnCycle );
	CHECK_RnB();

	NF_CMD ( CMD_READ_STATUS );
	Status = NF_RDDATA();


	return((Status & 1) ? CFALSE : CTRUE);
}


CBOOL fmd_WriteSector(
	unsigned long  startSectorAddr,   // page offset, 즉 block이랑 상관없이 page단위의 움직임이다.              
	unsigned char *pSectorBuff,       // buffer(sector수만큼 ==> page수만큼)                                    
	PSectorInfo    pSectorInfoBuff,   // spare area buffer, 위의 buffer와 or이다....즉, spare만도 읽을 수 있음. 
	unsigned int   dwNumSectors)      // sector 갯수.                                                           
{
	unsigned char Status;
	unsigned long SectorAddr = (unsigned long)startSectorAddr;
	unsigned int	dwByteCnt= 512;
	unsigned int		dwECC[ECC_SIZE];
	int i;
	unsigned int loop;

	
	if (!pSectorBuff && !pSectorInfoBuff) // 둘 다 NULL인 경우
		return(CFALSE);

	while (dwNumSectors--)
	{
		unsigned long blockPage	= SectorAddr;
		unsigned long DataOffset= 0, SpareOffset= 0;

		#if (NAND_LARGE_BLOCK==CTRUE)
		{
			SpareOffset	= NAND_PAGE_SIZE;
			dwByteCnt 	= NAND_PAGE_SIZE;
		}
		#endif
		
		CLEAR_RnB();			
			if (!pSectorBuff) // data write가 아닐 경우 ==> spare만 write할 경우
			{
				// If we are asked just to write the SectorInfo,
				// we will do that separately
				#if (NAND_LARGE_BLOCK==CTRUE)
				{
					NF_SETWRITE( blockPage, SpareOffset, ggnCycle );
				}
				#else
				{
					NF_SETWRITESPARE( blockPage, ggnCycle );
				}
				#endif
	
				for ( i=0; i<8; i++)
				{
					unsigned char ch;
	
					ch= *((unsigned char *)pSectorInfoBuff+i);
					NF_WRDATA(ch);
				}
				pSectorInfoBuff++;
			}
			else
			{

				//reset_ecc();
				
				NF_SETWRITE( blockPage, DataOffset , ggnCycle );
	
				for(loop = 0 ; loop < dwByteCnt/512 ; loop++)
				{
					//  Special case to handle un-aligned buffer pointer.
					if( ((unsigned int) pSectorBuff) & 0x3)
					{
	
						for ( i=0; i<512; i++)
						{
							unsigned char ch;
		
							ch= *((unsigned char *)pSectorBuff+i);
							NF_WRDATA(ch);
						}
	
					}
					else
					{
	
						for ( i=0; i<512/4; i++)
						{
							unsigned int ch;
					
							ch= *((unsigned int *)pSectorBuff+i);
							NF_WRDATA32(ch);
						}
	
					}
				
#if (NAND_ECC_CHK==CTRUE)	
					if (pSectorBuff)
					{			
						while(0 == (pECC->reg_NFECCSTATUS & NAND_WRENCDONE));
						
						dwECC[((loop * 2) + 0)] = pECC->reg_NFECCL;
						dwECC[((loop * 2) + 1)] = pECC->reg_NFECCH;
						
						gprintf("W pECC->reg_NFECCL = 0x%x, pECC->reg_NFECCH = 0x%x\n ", pECC->reg_NFECCL, pECC->reg_NFECCH);
						
						//gprintf("dwECC[((%d * 2) + 0)] = 0x%0x, dwECC[((%d * 2) + 1)] = 0x%0x\n", loop, dwECC[((loop * 2) + 0)], loop, dwECC[((loop * 2) + 1)] );
			

						#if (NAND_LARGE_BLOCK==CTRUE)
						{
							NF_SETWRITERANDOM((loop + 1) * 512);
						}
						#endif			
				
					}
#endif			
				pSectorBuff += 512;

				}
			}

		
		if (pSectorBuff)
		{
			// Write the SectorInfo data to the media.
			//
			#if (NAND_LARGE_BLOCK==CTRUE)
			{
				NF_SETWRITESPARE_SEQ( SpareOffset );
			}
			#else
			{
			}
			#endif

			if(pSectorInfoBuff)
			{
				for ( i=0; i<8; i++)
				{
					unsigned char ch;

					ch= *((unsigned char *)pSectorInfoBuff+i);
					NF_WRDATA(ch);
				}
				pSectorInfoBuff++;
			}
			else
			{
				// Make sure we advance the Flash's write pointer
				// (even though we aren't writing the SectorInfo data)
				unsigned char TempInfo[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
				for ( i=0; i<8; i++)
				{
					unsigned char ch;

					ch= *((unsigned char *)TempInfo+i);
					NF_WRDATA(ch);
				}
			}
	
		
#if (NAND_ECC_CHK==CTRUE)	
			{
				gprintf("write ecc.......\n");
				// Write the sector info and ECC into the spare area
				for( i=0; i<ECC_SIZE; i++) {
					NF_WRDATA32(dwECC[i]);
				}
			}
#endif
			//pSectorBuff += NAND_PAGE_SIZE;
		}

		NF_CMD( CMD_WRITE_CONFIRM );

		CHECK_RnB();

		++SectorAddr;
	}


	return(CTRUE);
}


void fmd_GetInfo(void)
{
#if 0	
	printf("wDataBytesPerSector = %d\n", NAND_PAGE_SIZE);
	printf("dwNumBlocks         = %d\n", NAND_BLOCK_CNT-gBlockOffset);
	printf("wSectorsPerBlock    = %d\n", NAND_PAGE_CNT);
	printf("dwBytesPerBlock		= %d\n", NAND_BLOCK_SIZE);
#endif
	
	printf("NAND_PAGE_SIZE      = %d\n", NAND_PAGE_SIZE);
	printf("NAND_BLOCK_CNT      = %d\n", NAND_BLOCK_CNT-gBlockOffset);
	printf("NAND_PAGE_CNT       = %d\n", NAND_PAGE_CNT);
	printf("NAND_BLOCK_SIZE     = %d\n", NAND_BLOCK_SIZE);
}

CBOOL IsBlockBad(unsigned int blockID)
{
	unsigned char		Data[16];
	unsigned long blockPage = (blockID * NAND_PAGE_CNT);


	CLEAR_RnB();
//	NF_CMD(CMD_RESET);
//	CHECK_RnB();

	// Read Spare
	fmd_ReadSector( (unsigned long)blockPage, NULL, (PSectorInfo)Data, 1);

	if(0xff != Data[5])
	{
		return(CTRUE);
	}

	return(CFALSE);
}

int fmd_GetBlockStatus(unsigned int blockID)
{
	unsigned long Sector = (blockID * NAND_PAGE_CNT);
	SectorInfo SI;
	unsigned int dwResult = 0;

	gprintf("fmd_GetBlockStatus %d\n",blockID);

	if( IsBlockBad(blockID) ) 
	{
		gprintf("fmd_GetBlockStatus BLOCK_STATUS_BAD\n");

		return -1;
	}

	if (!fmd_ReadSector(Sector, NULL, &SI, 1))
	{
		gprintf("fmd_GetBlockStatus BLOCK_STATUS_UNKNOWN\n");
		return -2;
	}

	gprintf("fmd_GetBlockStatus %d return\n",blockID );

	return(dwResult);
}


CBOOL MarkBlockBad(unsigned int blockID)
{
	unsigned char	Data[8]= {0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff};
	CBOOL	bRet;
	unsigned long 	blockPage = (blockID * NAND_PAGE_CNT);

	CLEAR_RnB();
//	NF_CMD(CMD_RESET);			// Send reset command.
//	CHECK_RnB();

	// Read Spare
	bRet= fmd_WriteSector( (unsigned long)blockPage, NULL, (PSectorInfo)Data, 1);

	return bRet;
}

#if 0
CBOOL fmd_SetBlockStatus(unsigned int blockID, unsigned int dwStatus)
{
	if (dwStatus & BLOCK_STATUS_BAD)
	{
		if (!MarkBlockBad(blockID)) {
			return(CFALSE);
		}
	}

	if (dwStatus & (BLOCK_STATUS_READONLY | BLOCK_STATUS_RESERVED)) 
	{

		unsigned long Sector = blockID * NAND_PAGE_CNT;
		SectorInfo SI;

		if (!fmd_ReadSector(Sector, NULL, &SI, 1)) {
			return CFALSE;
		}

		if (dwStatus & BLOCK_STATUS_READONLY) {
			SI.bOEMReserved &= ~OEM_BLOCK_READONLY;
		}

		if (dwStatus & BLOCK_STATUS_RESERVED) {
			SI.bOEMReserved &= ~OEM_BLOCK_RESERVED;
		}

		if (!fmd_WriteSector (Sector, NULL, &SI, 1)) {
			return CFALSE;
		}

	}

	return(CTRUE);
}
#endif


void fmd_checkid(void)
{
	unsigned char ch;
	int i;
	
	CLEAR_RnB();
	NF_CMD(CMD_READ_ID);		// Send reset command.
	NF_ADDR(0x0)	
	
	
	for(i=0; i<10; i++);
	
	ch= NF_RDDATA();
	
	printf("maf: 0x%02x\n", ch);
	ch= NF_RDDATA();
	printf("dev: 0x%02x\n", ch);
	
	CLEAR_RnB();
}
	
	
	

int do_nandE( cmd_tbl_t *cmdtp, int flag, int argc, char *argv[] )
{
	int startblk, size, eraseblocks, i;
	CBOOL ret;

	if ( argc != 3 )
	{
    	printf ("Usage:\n%s\n", cmdtp->help);
        return 1;
    }
    
    if(flag)
    {
        startblk = CONFIG_FILSYSTEM_STARTBLK_NO;
        size     = 0x40000000 - ( NAND_BLOCK_SIZE * startblk);
    }
    else
    {
        startblk = simple_strtoul(argv[1], NULL, 10);
        size     = simple_strtoul(argv[2], NULL, 16);
    }
    
  	printf("StartBlock %d (0x%x) : Size %d (0x%x) \n", startblk, startblk, size, size);
    
    eraseblocks = size / NAND_BLOCK_SIZE;
    printf("Total Erase Blocks %d (0x%x) \n",eraseblocks, eraseblocks);


    for ( i=0 ; i < eraseblocks;  i++) 
    {
        ret = fmd_EraseBlock(startblk);
        if( ret == CFALSE )
        {
        	printf("Block erase error: %d blocks\n", startblk);
        }
        startblk++;
    }
    
	return 0;
}


U_BOOT_CMD(
	nande,	4,	1,	do_nandE,
       "nande	- delete all block \n",
       "[startblock in decimal] [size in hex] \n"		\
);



int do_nandw    (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	ulong	startblk, size, memadr, startpage, numofpages;
	CBOOL ret;

	if (argc !=  4) 
	{
		printf ("Usage:\n%s\n", cmdtp->help);
		return 1;
	}

	startblk = simple_strtoul(argv[1], NULL, 10);
	size     = simple_strtoul(argv[2], NULL, 16);
	memadr   = simple_strtoul(argv[3], NULL, 16);
	
	startpage = startblk*NAND_PAGE_CNT; // block당 page 갯수만큼 곱하기 하면 해당 block의 시작 page가 나온다.
	
	numofpages = size/NAND_PAGE_SIZE;
	
	if( size%NAND_PAGE_SIZE )
		numofpages++;
		
	printf("startpage: %d(%d*%d), numofpages: %d, memaddr = 0x%x\n", startpage, startblk, NAND_PAGE_CNT, numofpages, memadr);

	ret = fmd_WriteSector(startpage, (unsigned char *)memadr, NULL, numofpages );
    if( ret == CFALSE )
    {
        printf("write error\n");
    }
	
	return 0;
}


U_BOOT_CMD(
	nandw,	4,	1,	do_nandw,
       "nandw	- NAND Flash Write Program \n",
       "[startblock in decimal] [size in hex] [memaddr in hex] \n"		\
);


int do_nandr    (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	ulong	startblk, size, memadr, startpage, numofpages;
	CBOOL ret;
	
	if(argc !=  4) 
	{
		printf ("Usage:\n%s\n", cmdtp->help);
		return 1;
	}

	startblk = simple_strtoul(argv[1], NULL, 0);
	size     = simple_strtoul(argv[2], NULL, 16);
	memadr   = simple_strtoul(argv[3], NULL, 16);

	startpage = startblk*NAND_PAGE_CNT; // block당 page 갯수만큼 곱하기 하면 해당 block의 시작 page가 나온다.

	numofpages = size/NAND_PAGE_SIZE;
	
	if( size%NAND_PAGE_SIZE )
		numofpages++;
		
	printf("startpage: %d(%d*%d), numofpages: %d, memaddr = 0x%x\n", startpage, startblk, NAND_PAGE_CNT, numofpages, memadr);

	ret = fmd_ReadSector(startpage, (unsigned char *)memadr, NULL, numofpages );
    if( ret == CFALSE )
    {
        printf("read error\n");
    }

	return 0;
}


U_BOOT_CMD(
	 nandr,	4,	1,	do_nandr,
       "nandr	- NAND Flash Read Program \n",
       "[startblock in decimal] [size in hex] [memaddr in hex] \n"		\
);
