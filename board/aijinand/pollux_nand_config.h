
#ifndef __NAND_CONFIG_H__
#define __NAND_CONFIG_H__

//#include "wincetype.h"

#if 1
	#define gprintf(fmt, x... ) printf( "%s: " fmt, __FUNCTION__ , ## x)
#else
	#define gprintf(x...) do { } while (0)
#endif


#define PHY_BASEADDR_NANDECC			0xC0015874

typedef volatile struct _tag_NAND_ECC 
{
	volatile U32	reg_NFCONTROL;			// 0x5874
	volatile U32	reg_NFECCL;				// 0x5878
	volatile U32	reg_NFECCH;				// 0x587c
	volatile U32	reg_NFORGECCL;			// 0x5880
	volatile U32	reg_NFORGECCH;			// 0x5884
	volatile U32	reg_NFCNT;				// 0x5888
	volatile U32	reg_NFECCSTATUS;		// 0x588c
	volatile U32	reg_NFSYNDRONE31;		// 0x5890
	volatile U32	reg_NFSYNDRONE75;		// 0x5894
} NANDECC , *PNANDECC;


typedef volatile struct _tag_NAND 
{
	volatile U32	NFDATA;		// 0x00
	U32		dummy0[3];

	volatile U8	NFCMD ;		// 0x10
	U8		dummy1[3];
	U32		dummy2;

	volatile U8	NFADDR;		// 0x18
} NANDOFFSET, *PNANDOFFSET;


#define	CMD_READ_SPARE						0x50		//	Read2 for Spare
#define	CMD_ERASE							0x60
#define	CMD_ERASE_CONFIRM					0xd0
#define	CMD_READ_ID							0x90
#define	CMD_READ							0x00
#define	CMD_RESET							0xff
#define	CMD_WRITE							0x80
#define	CMD_WRITE_CONFIRM					0x10
#define	CMD_READ_CONFIRM					0x30		// only for Large block
#define	CMD_READ_STATUS						0x70
#define	CMD_RANDOM_DATA_OUTPUT				0x05
#define	CMD_RANDOM_DATA_OUTPUT_CONFIRM		0xe0
#define	CMD_RANDOM_DATA_INPUT				0x85
#define	CMD_READ_FOR_COPY_BACK				0x35
#define	CMD_CACHE_PROGRAM					0x15


#define PNL_3CYCLES				3
#define PNL_4CYCLES				4
#define PNL_5CYCLES				5
	
#define STATUS_READY						0x40		//  Ready
#define STATUS_ERROR						0x01		//  Error



#define	NF_CMD(cmd)				{pNANDOffset->NFCMD	=  (U8)(cmd);}
#define	NF_ADDR(addr)			{pNANDOffset->NFADDR=  (U8)(addr);}
#define	NF_RDDATA()				(*(volatile U8*)(&pNANDOffset->NFDATA))
#define	NF_RDDATA32()			(pNANDOffset->NFDATA)
#define	NF_WRDATA(data)			{*(volatile U8*)(&pNANDOffset->NFDATA)=  (U8)(data);}
#define	NF_WRDATA32(data)		{pNANDOffset->NFDATA=  (data);}
#define CLEAR_RnB()		pECC->reg_NFCONTROL |= 0x8000
#define CHECK_RnB()		while(1)								\
	{															\
		if (pECC->reg_NFCONTROL & 0x8000)							\
		{														\
			CLEAR_RnB();										\
			break;												\
		}														\
	}		


#define	NF_WAITB()				{	NF_CMD( CMD_READ_STATUS );			\
									while (!(NF_RDDATA() & 0x40)); }	


// ghcstop add
#define NAND_LARGE_BLOCK (CTRUE)
#define NAND_MLC
#define NAND_SIZE_UNIT_MB (1024)
#define NAND_ECC_CHK (CTRUE)



#if (NAND_LARGE_BLOCK==CFALSE)
	#if (NAND_SIZE_UNIT_MB<64)
		#define NAND_ADDRESS_TYPE	PNL_3CYCLES
	#else
		#define NAND_ADDRESS_TYPE	PNL_4CYCLES
	#endif

	#define NAND_SMALL_ADDRESSING	PNL_3CYCLES
	#define NAND_LARGE_ADDRESSING	PNL_4CYCLES

	#define NAND_BLOCK_CNT			(U32)((float)NAND_SIZE_UNIT_MB/(1./64.))
	#define NAND_PAGE_CNT 			(32)            /* Each Block has 32 Pages      */
	#define NAND_PAGE_SIZE			(512)           /* Each Page has 512 Bytes      */
	#define NAND_BLOCK_SIZE			(NAND_PAGE_CNT * NAND_PAGE_SIZE)

#else

	#define NAND_SMALL_ADDRESSING	PNL_4CYCLES
	#define NAND_LARGE_ADDRESSING	PNL_5CYCLES

	#if defined(NAND_MLC)
	//#  error "MLC"	
		#define NAND_ADDRESS_TYPE	PNL_5CYCLES
	
		#define NAND_PAGE_CNT           128		// Num of Logical Page
		#define NAND_PAGE_SIZE          2048	// Logical Page= 2k = 2048

		//#define NAND_BLOCK_CNT          (U32)((float)NAND_SIZE_UNIT_MB*(1024*1024/128/4096))
		//#define NAND_BLOCK_CNT          (U32)((float)NAND_SIZE_UNIT_MB*(1024*1024/128/2048))
		#define NAND_BLOCK_CNT          (U32)((float)NAND_SIZE_UNIT_MB*(1024*1024/NAND_PAGE_CNT/NAND_PAGE_SIZE))
		#define NAND_BLOCK_SIZE         (NAND_PAGE_CNT * NAND_PAGE_SIZE)
	
	#else
	//#  error "no MLC"	
		#if (NAND_SIZE_UNIT_MB<256)
			#define NAND_ADDRESS_TYPE	PNL_4CYCLES
		#else
			#define NAND_ADDRESS_TYPE	PNL_5CYCLES
		#endif
	
		#if (FORCE_512==CTRUE)
			#define NAND_PAGE_CNT           64*4//(64*4)		// Num of Logical Page
			#define NAND_PAGE_SIZE          512//(2048/4)	// Logical Page= 512
		#else
			#define NAND_PAGE_CNT           64//(64*4)		// Num of Logical Page
			#define NAND_PAGE_SIZE          2048//(2048/4)	// Logical Page= 512
		#endif
		#define NAND_BLOCK_CNT          (U32)((float)NAND_SIZE_UNIT_MB/(1./8.))
		#define NAND_BLOCK_SIZE         (NAND_PAGE_CNT * NAND_PAGE_SIZE)
	#endif

#endif



#if (NAND_LARGE_BLOCK==CFALSE)

	#define NF_SETREAD( nPage, nOffset, nCycles, bChkWait )				\
			{															\
				NF_CMD	( CMD_READ );									\
			    NF_ADDR	( ((nOffset)     )  & 0xff );					\
				NF_ADDR	( ((gPageOffset+nPage)       )  & 0xff );		\
			    NF_ADDR	( ((gPageOffset+nPage)   >> 8)  & 0xff );		\
				if (nCycles == PNL_4CYCLES)								\
					NF_ADDR( ((gPageOffset+nPage) >> 16) & 0xff );		\
				if (bChkWait==CTRUE)	{ CHECK_RnB(); }					\
			}

	#define NF_SETREADSPARE( nPage, nCycles )							\
			{                         									\
				NF_CMD	( CMD_READ_SPARE );								\
			    NF_ADDR	( 0 );											\
				NF_ADDR	( ((gPageOffset+nPage)       )  & 0xff );		\
			    NF_ADDR	( ((gPageOffset+nPage)   >> 8)  & 0xff );		\
				if (nCycles == PNL_4CYCLES)								\
					NF_ADDR( ((gPageOffset+nPage) >> 16) & 0xff );		\
				CHECK_RnB ();											\
			}

	#define NF_SETWRITE( nPage, nOffset, nCycles )						\
			{															\
				NF_CMD	( CMD_READ );									\
				NF_CMD	( CMD_WRITE );									\
			    NF_ADDR	( ((nOffset)     )  & 0xff );					\
				NF_ADDR	( ((gPageOffset+nPage)       )  & 0xff );		\
			    NF_ADDR	( ((gPageOffset+nPage)   >> 8)  & 0xff );		\
				if (nCycles == PNL_4CYCLES)								\
					NF_ADDR( ((gPageOffset+nPage) >> 16) & 0xff );		\
			}

	#define NF_SETWRITESPARE( nPage, nCycles )							\
			{                                                           \
				NF_CMD	( CMD_READ_SPARE );								\
				NF_CMD	( CMD_WRITE );									\
			    NF_ADDR	( 0 );											\
				NF_ADDR	( ((gPageOffset+nPage)       )  & 0xff );		\
			    NF_ADDR	( ((gPageOffset+nPage)   >> 8)  & 0xff );		\
				if (nCycles == PNL_4CYCLES)								\
					NF_ADDR( ((gPageOffset+nPage) >> 16) & 0xff );		\
			}

	#define NF_SETERASE( nBlock, nCycles )								\
			{															\
				U32 nPage = ( (gBlockOffset+nBlock) <<5 );				\
				NF_CMD	( CMD_ERASE );									\
				NF_ADDR	( ((nPage)       )  & 0xff );					\
			    NF_ADDR	( ((nPage)   >> 8)  & 0xff );					\
				if (nCycles == PNL_4CYCLES)								\
					NF_ADDR( ((nPage) >> 16) & 0xff );					\
			    NF_CMD	( CMD_ERASE_CONFIRM );							\
			}

#else //#if (NAND_LARGE_BLOCK==CFALSE) ==> TRUE case

	#define NF_SETREAD(nPage, nOffset, nCycles, bChkWait)				\
			{															\
				NF_CMD	( CMD_READ );									\
			    NF_ADDR	( ((nOffset)     )  & 0xff);					\
				NF_ADDR	( ((nOffset) >> 8)  & 0xff);					\
				NF_ADDR	( ((gPageOffset+nPage)       )  & 0xff);		\
			    NF_ADDR	( ((gPageOffset+nPage)   >> 8)  & 0xff);		\
				if (nCycles == PNL_5CYCLES)								\
					NF_ADDR( ((gPageOffset+nPage) >> 16) & 0xff);		\
				NF_CMD	( CMD_READ_CONFIRM );							\
				if (bChkWait==CTRUE)	{ CHECK_RnB() }						\
			}

	#define NF_SETREADSPARE_SEQ( Offset )								\
			{                                                           \
				NF_CMD	( CMD_RANDOM_DATA_OUTPUT );						\
				NF_ADDR	( ((Offset)       )  & 0xff);					\
			    NF_ADDR	( ((Offset)   >> 8)  & 0xff);					\
			    NF_CMD	( CMD_RANDOM_DATA_OUTPUT_CONFIRM );				\
			}

	#define NF_SETWRITERANDOM(nOffset)						\
			{															\
				NF_CMD	( CMD_RANDOM_DATA_INPUT );									\
			    NF_ADDR	( ((nOffset)     )  & 0xff);					\
				NF_ADDR	( ((nOffset) >> 8)  & 0xff);					\
			}

	#define NF_SETWRITE(nPage, nOffset, nCycles)						\
			{															\
				NF_CMD	( CMD_WRITE );									\
			    NF_ADDR	( ((nOffset)     )  & 0xff);					\
				NF_ADDR	( ((nOffset) >> 8)  & 0xff);					\
				NF_ADDR	( ((gPageOffset+nPage)       )  & 0xff);		\
			    NF_ADDR	( ((gPageOffset+nPage)   >> 8)  & 0xff);		\
				if (nCycles == PNL_5CYCLES)								\
					NF_ADDR( ((gPageOffset+nPage) >> 16) & 0xff);		\
			}
	#define NF_SETWRITESPARE_SEQ( Offset )								\
			{                                                           \
				NF_CMD	( CMD_RANDOM_DATA_INPUT );                      \
				NF_ADDR	( ((Offset)       )  & 0xff);					\
			    NF_ADDR	( ((Offset)   >> 8)  & 0xff);					\
			}
	#define NF_SETERASE( nBlock, nCycles )								\
			{                                                           \
				U32 nPage = ( (gBlockOffset+nBlock) << 6 );				\
				NF_CMD	( CMD_ERASE );									\
				NF_ADDR	( ((nPage)       )  & 0xff );					\
			    NF_ADDR	( ((nPage)   >> 8)  & 0xff );					\
				if (nCycles == PNL_5CYCLES)								\
					NF_ADDR( ((nPage) >> 16) & 0xff );					\
			    NF_CMD	( CMD_ERASE_CONFIRM );							\
			}

#endif // #if (NAND_LARGE_BLOCK==CFALSE)



typedef struct _SectorInfo {
  unsigned int   dwReserved1;
  unsigned char  bOEMReserved;
  unsigned char  bBadBlock;
  unsigned short wReserved2;
} SectorInfo, *PSectorInfo;




void ReadOddSyndrome( int  *s );
int		MES_NAND_GetErrorLocation(  int  *s, int *pLocation );
void *fmd_Init(void);
void fmd_ReadECCBypass(int bypass);
CBOOL fmd_ReadSector(unsigned long  startSectorAddr,unsigned char *pSectorBuff,    PSectorInfo    pSectorInfoBuff,unsigned int   dwNumSectors); 
CBOOL fmd_EraseBlock(unsigned int blockID); // block number
CBOOL fmd_WriteSector(	unsigned long  startSectorAddr,	unsigned char *pSectorBuff,	PSectorInfo    pSectorInfoBuff,	unsigned int   dwNumSectors);
void fmd_GetInfo(void);
CBOOL IsBlockBad(unsigned int blockID);
int fmd_GetBlockStatus(unsigned int blockID);
CBOOL MarkBlockBad(unsigned int blockID);
void fmd_checkid(void);




#endif // __NAND_CONFIG_H__

