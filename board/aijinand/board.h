//------------------------------------------------------------------------------
//
//	Copyright (C) 2005 MagicEyes Digital Co., Ltd All Rights Reserved
//	MagicEyes Digital Co. Proprietary & Confidential
//
//	MAGICEYES INFORMS THAT THIS CODE AND INFORMATION IS PROVIDED "AS IS" BASE 
//  AND WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT 
//  NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR 
//  A PARTICULAR PURPOSE.
//
//	Author     : Neo Kang (neo@mesdigital.com)
//	History    :
//	   2005/08/02 Neo first implementation
//------------------------------------------------------------------------------
#ifndef _BOARD_H
#define _BOARD_H

#ifdef __cplusplus
	extern "C" {
#endif

void BOARD_Initialize(void);

#define	LCD_nCS_BIT		12
#define	LCD_SCK_BIT		13
#define	LCD_SDO_BIT		14	// SPI RX : Connected ILI9322 SPI TX
#define	LCD_SDI_BIT		15	// SPI TX : Connected ILI9322 SPI RX
#define	LCD_nRES_BIT		5	// Connected ILI9322 reset
#define LCD_nCS_GPIO		1
#define LED_PWM_GPIO		0	// PAD_GPIO_A
#define LED_PWM_BIT		30

#define	SDI_LOW		MES_GPIO_SetOutputValue( LCD_SDI_BIT, CFALSE )
#define SDI_HIGH	MES_GPIO_SetOutputValue( LCD_SDI_BIT, CTRUE  )	
#define	CSB_LOW		MES_GPIO_SetOutputValue( LCD_nCS_BIT, CFALSE )
#define CSB_HIGH	MES_GPIO_SetOutputValue( LCD_nCS_BIT, CTRUE  )	
#define	SCL_LOW		MES_GPIO_SetOutputValue( LCD_SCK_BIT, CFALSE )
#define SCL_HIGH	MES_GPIO_SetOutputValue( LCD_SCK_BIT, CTRUE  )	
#define SDO_VALUE	MES_GPIO_GetInputValue( LCD_SDO_BIT )
#define DELAY		CountDelay(1)

#define	ILI9322_ID		0x96

#ifdef __cplusplus
};
#endif

#endif // _BOARD_H
